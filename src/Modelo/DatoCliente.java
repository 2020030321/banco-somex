
package Modelo;


public class DatoCliente {
    
    private String nombreCli, fechaNac, domicilio, sexo;

    public DatoCliente(String nombreCli, String fechaNac, String domicilio, String sexo) {
        this.nombreCli = nombreCli;
        this.fechaNac = fechaNac;
        this.domicilio = domicilio;
        this.sexo = sexo;
    }
    
    public DatoCliente(DatoCliente ob) {
        this.nombreCli = ob.nombreCli;
        this.fechaNac = ob.fechaNac;
        this.domicilio = ob.domicilio;
        this.sexo = ob.sexo;
    }
    
    public DatoCliente() {
        this.nombreCli = "";
        this.fechaNac = "";
        this.domicilio = "";
        this.sexo = "";
    }
    
    //SETS
    public void setNombreCli(String nombreCli) {
        this.nombreCli = nombreCli;
    }

    public void setFechaNac(String fechaNac) {
        this.fechaNac = fechaNac;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }
    
    //GETS

    public String getNombreCli() {
        return nombreCli;
    }

    public String getFechaNac() {
        return fechaNac;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public String getSexo() {
        return sexo;
    }
    
    
    
    
}
