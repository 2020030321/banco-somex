
package Modelo;


public class Cuenta {
    
    private int numCuenta;
    private String fechaAPer, nombreBanco;
    private float saldo, porceRendi;
    private DatoCliente datoc;
    
    //CONSTRUCTOR VALORES NUEVOS
    public Cuenta(int numCuenta, String fechaAPer, String nombreBanco, float saldo, float porceRendi, DatoCliente datoc) {
        this.numCuenta = numCuenta;
        this.fechaAPer = fechaAPer;
        this.nombreBanco = nombreBanco;
        this.saldo = saldo;
        this.porceRendi = porceRendi;
        this.datoc = datoc;
    }
    
    //CONSTRUCTOR CLONAR OBJETO
    public Cuenta(Cuenta ob, DatoCliente obc) {
        this.numCuenta = ob.numCuenta;
        this.fechaAPer = ob.fechaAPer;
        this.nombreBanco = ob.nombreBanco;
        this.saldo = ob.saldo;
        this.porceRendi = ob.porceRendi;
        this.datoc = obc;
    }
    
    //CONSTRUCTOR VACIO
    public Cuenta() {
        this.numCuenta = 0;
        this.fechaAPer = "";
        this.nombreBanco = "";
        this.saldo = 0;
        this.porceRendi = 0;
        datoc = new DatoCliente();
    }
    
    //SETS
    public void setNumCuenta(int numCuenta) {
        this.numCuenta = numCuenta;
    }

    public void setFechaAPer(String fechaAPer) {
        this.fechaAPer = fechaAPer;
    }

    public void setNombreBanco(String nombreBanco) {
        this.nombreBanco = nombreBanco;
    }

    public void setSaldo(float saldo) {
        this.saldo = saldo;
    }

    public void setPorceRendi(float porceRendi) {
        this.porceRendi = porceRendi;
    }

    public void setDatoc(DatoCliente datoc) {
        this.datoc = datoc;
    }
    
    
    //GETS
    public int getNumCuenta() {
        return numCuenta;
    }

    public String getFechaAPer() {
        return fechaAPer;
    }

    public String getNombreBanco() {
        return nombreBanco;
    }

    public float getSaldo() {
        return saldo;
    }

    public float getPorceRendi() {
        return porceRendi;
    }

    public DatoCliente getDatoc() {
        return datoc;
    }
    
    
    //METODO DEPOSITAR
    public void depositar(float dep){
        saldo += dep;
    }
    
    ////METODO RETIRAR
    public boolean retirar(float ret){
        if(ret<=saldo){
            saldo-=ret;
            return true;
        }else if(ret>saldo){
            return false;
        }else if(ret==0){
            return false;
        }else 
            return false;
    }
    
    //METODO CALUCLAR RENDIMIENTO DIARIO
    public float calcularReditos(){
        return ((porceRendi*saldo)/365);
    }
    
}
