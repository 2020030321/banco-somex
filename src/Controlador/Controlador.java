
package Controlador;

import Modelo.Cuenta;
import Modelo.DatoCliente;
import Vista.dlgBanco;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JOptionPane;


public class Controlador implements ActionListener {
    
    private Cuenta cue;
    private DatoCliente dac;
    private dlgBanco vista;

    public Controlador(Cuenta cue, dlgBanco vista) {
        this.cue = cue;
        this.vista = vista;
        
        vista.btnNuevo.addActionListener(this);
        vista.btnDepositarMonto.addActionListener(this);
        vista.btnExit.addActionListener(this);
        vista.btnGuardar.addActionListener(this);
        vista.btnMostrar.addActionListener(this);
        vista.btnRetirarMonto.addActionListener(this);
        vista.rbFemenino.addActionListener(this);
        vista.rbMasculino.addActionListener(this);
        
        
    }
    
    public void iniciarVista(){
        vista.setTitle("BANCO NACIONAL SOMEX");
        vista.setSize(600, 500);
        vista.setVisible(true);
        
    }
    
    

    public static void main(String[] args) {
    
        dlgBanco vista = new dlgBanco(new JFrame(), true);
        Cuenta cue = new Cuenta();
        Controlador con = new Controlador(cue,vista);
        con.iniciarVista();
        
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        
        if(e.getSource()==vista.btnNuevo){
            vista.txtCantidadMonto.setEnabled(true);
            vista.txtDomicilioCliente.setEnabled(true);
            vista.txtFechaAperturaCuenta.setEnabled(true);
            vista.txtFechaNacimientoCliente.setEnabled(true);
            vista.txtNombreBanco.setEnabled(true);
            vista.txtNombreCliente.setEnabled(true);
            vista.txtNuevoSaldoCuenta.setEnabled(true);
            vista.txtNumCuenta.setEnabled(true);
            vista.txtPorcentajeRendimiento.setEnabled(true);
            vista.txtSaldoCuenta.setEnabled(true);
            
            vista.btnGuardar.setEnabled(true);
            vista.btnMostrar.setEnabled(true);
            vista.btnDepositarMonto.setEnabled(true);
            vista.btnRetirarMonto.setEnabled(true);
            
            vista.rbFemenino.setEnabled(true);
            vista.rbMasculino.setEnabled(true);
            
            vista.txtCantidadMonto.setText("");
            vista.txtDomicilioCliente.setText("");
            vista.txtFechaAperturaCuenta.setText("");
            vista.txtFechaNacimientoCliente.setText("");
            vista.txtNombreBanco.setText("");
            vista.txtNombreCliente.setText("");
            vista.txtNuevoSaldoCuenta.setText("");
            vista.txtNumCuenta.setText("");
            vista.txtPorcentajeRendimiento.setText("");
            vista.txtSaldoCuenta.setText("");
            
            vista.rbFemenino.setSelected(false);
            vista.rbMasculino.setSelected(false);
            
            
        }
        
        if(e.getSource()==vista.rbFemenino){
            vista.rbMasculino.setSelected(false);
        }
        
        if(e.getSource()==vista.rbMasculino){
            vista.rbFemenino.setSelected(false);
        }
        
        if(e.getSource()==vista.btnGuardar){
            
            try{
                cue.setNumCuenta(Integer.parseInt(vista.txtNumCuenta.getText()));
                cue.setPorceRendi(Float.parseFloat(vista.txtPorcentajeRendimiento.getText()));
                cue.setSaldo(Float.parseFloat(vista.txtSaldoCuenta.getText()));
            }
            catch(NumberFormatException ex){
                JOptionPane.showMessageDialog(vista, "Surgió el siguiente error: " + ex.getMessage());
                return;
            }
            catch(Exception ex2){
                
                JOptionPane.showMessageDialog(vista, "Surgió el siguiente error: " + ex2.getMessage());
                return;
            }
            String sex = "";
            if(vista.rbFemenino.isSelected()==true){
                    sex="Femenino";
                } else if(vista.rbMasculino.isSelected()==true){
                    sex="Masculino";
                } else if (vista.rbFemenino.isSelected()==false && vista.rbMasculino.isSelected()==false){
                    JOptionPane.showMessageDialog(vista, "Seleccione su sexo. " );
                    return;
                }
            
            try{
                cue.setNombreBanco(vista.txtNombreBanco.getText());
                cue.setFechaAPer(vista.txtFechaAperturaCuenta.getText());
                    
                cue.setDatoc(
                        new DatoCliente(
                                vista.txtNombreCliente.getText(),
                                vista.txtFechaNacimientoCliente.getText(),
                                vista.txtDomicilioCliente.getText(),
                                    sex
                        )
                
                );
                
            }
            catch(Exception ex2){
                
                JOptionPane.showMessageDialog(vista, "Surgió el siguiente error: " + ex2.getMessage());
                return;
            }
            
            JOptionPane.showMessageDialog(vista, "Datos guardados exitosamente  " );
                return;
            
        }
        
        if(e.getSource()==vista.btnMostrar){
            vista.txtNumCuenta.setText(String.valueOf(cue.getNumCuenta()));
            vista.txtNombreCliente.setText(cue.getDatoc().getNombreCli());
            vista.txtDomicilioCliente.setText(cue.getDatoc().getDomicilio());
            vista.txtFechaNacimientoCliente.setText(cue.getDatoc().getFechaNac());
            
            vista.txtNombreBanco.setText(cue.getNombreBanco());
            vista.txtFechaAperturaCuenta.setText(cue.getFechaAPer());
            vista.txtPorcentajeRendimiento.setText(String.valueOf(cue.getPorceRendi()));
            vista.txtSaldoCuenta.setText(String.valueOf(cue.getSaldo()));
                        
            
            if("Masculino".equals(cue.getDatoc().getSexo())){
                vista.rbMasculino.setSelected(true);
                vista.rbFemenino.setSelected(false);
            }else if("Femenino".equals(cue.getDatoc().getSexo())){
                vista.rbFemenino.setSelected(true);
                vista.rbMasculino.setSelected(false);
            }
        }
        
        if(e.getSource()==vista.btnDepositarMonto){
            
            float monto;
            
            try{
                
                monto =Float.parseFloat( vista.txtCantidadMonto.getText());
                cue.depositar(monto);
                vista.txtNuevoSaldoCuenta.setText(String.valueOf(cue.getSaldo()));
            }
            catch(NumberFormatException ex){
                JOptionPane.showMessageDialog(vista, "Surgió el siguiente error: " + ex.getMessage());
                return;
            }
            catch(Exception ex2){
                
                JOptionPane.showMessageDialog(vista, "Surgió el siguiente error: " + ex2.getMessage());
                return;
            }
            
            JOptionPane.showMessageDialog(vista, "El monto de  $" + monto + " fue depositado exitosamente. ");
                return;
            
            
        }
        
        if(e.getSource()==vista.btnRetirarMonto){
            
            float monto;
            
            try{
                monto =Float.parseFloat( vista.txtCantidadMonto.getText());
                if(cue.retirar(monto)==true){
                    JOptionPane.showMessageDialog(vista, "Retiro exitoso. \n No olvide consultar su nuevo saldo. ");
                    vista.txtNuevoSaldoCuenta.setText(String.valueOf(cue.getSaldo()));
                    return;
                }
                else if(cue.retirar(monto)==false){
                    int option = JOptionPane.showConfirmDialog(vista, "No posee fondos suficientes, ¿Desea volver a intentar?   ",
                        "Seleccione", JOptionPane.YES_NO_OPTION);
                    if(option == JOptionPane.NO_OPTION){
                        vista.dispose();
                        System.exit(0);
                        }
                    else if (option == JOptionPane.YES_OPTION){
                        vista.txtCantidadMonto.setText("");
                        return;
                    }
                    
                }
                
                
            }catch(NumberFormatException ex){
                JOptionPane.showMessageDialog(vista, "Surgió el siguiente error: " + ex.getMessage());
                return;
            }
            catch(Exception ex2){
                
                JOptionPane.showMessageDialog(vista, "Surgió el siguiente error: " + ex2.getMessage());
                return;
            }
            
            
            
            
        }
        
        
        if(e.getSource()==vista.btnExit){
                int option = JOptionPane.showConfirmDialog(vista, "¿Desea salir?",
                        "Seleccione", JOptionPane.YES_NO_OPTION);
                if(option == JOptionPane.YES_NO_OPTION){
                    vista.dispose();
                    System.exit(0);
            }
        }
        
    }
    
}
